package day8;

import java.util.Comparator;

public class AlphabetAppleComparator implements Comparator<Apple> {
    @Override
    public int compare(Apple o1, Apple o2) {
        return o1.sort.compareTo(o2.sort);
    }
}
