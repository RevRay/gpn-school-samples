package day8;

import java.util.*;

public class PG {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.addAll(List.of(1, 45, -90, 23, 8, 0 -112));

        //Array  Arrays.sort swap shuffle
        //Collection
        Collections.sort(list);
        System.out.println(list);




        String s1 = "abc"; //byte[]
        String s2 = "cbf";

        List<Apple> list2 = new ArrayList<>();
        list2.add(new Apple(130, "green"));
        list2.add(new Apple(135, "red"));
        list2.add(new Apple(135, "blue"));
        list2.add(new Apple(120, "fedzi"));

        System.out.println(list2);
        Collections.sort(list2);
        System.out.println(list2);

//        Collections.sort(list2, new AlphabetAppleComparator());
//        list2.sort(new AlphabetAppleComparator());
        list2.sort(new Comparator<Apple>() {
            @Override
            public int compare(Apple o1, Apple o2) {
                return o1.sort.compareTo(o2.sort);
            }
        });

        System.out.println(list2);

        Comparator<Apple> c1 = new Comparator<Apple>() {
            @Override
            public int compare(Apple o1, Apple o2) {
                return o1.sort.length() - o2.sort.length();
            }
        };
//        list2.sort(new SortLengthAppleComparator());
        System.out.println(list2);

    }
}
