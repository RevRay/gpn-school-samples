package day8;

import java.util.Comparator;

public class Planet {
    public static void main(String[] args) {
        RelatedClass r = new RelatedClass();

//        AuxiliaryClass a = new AuxiliaryClass();
        Planet p = new Planet();
        Satellite s = p.new Satellite();
        NeighbourClass n = new NeighbourClass();

        class LocalClass {
            int size;
            int name;

            public LocalClass(int size, int name) {
                this.size = size;
                this.name = name;
            }
        }

        LocalClass l = new LocalClass(12, 12);

        Comparator c = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                return 0;
            }
        };

        System.out.println(p.getClass());
        System.out.println(n.getClass());
        System.out.println(r.getClass());
        System.out.println(s.getClass());
        System.out.println(l.getClass());
        System.out.println(c.getClass());
    }

    public static void anotherMethod() {
        Comparator c = new Comparator() { //анонимный класс на базе интерфейса компаратор
            @Override
            public int compare(Object o1, Object o2) {
                return 0;
            }
        };
    }


    public static class RelatedClass {
        static int p;
    }

    public class Satellite {

    }
}
class NeighbourClass {

}

