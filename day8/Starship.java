package day8;

public class Starship {

    public static void main(String[] args) {
        Starship s = new Starship("Kerosine", 10000);
        s.setFuel("Heptil");
        s.getFuel();

        s.fuel = "";

        System.out.println(s.getFuel());
    }

    String fuel;
    int speed;

    public Starship(String fuel, int speed) {
        this.fuel = fuel;
        this.speed = speed;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}
