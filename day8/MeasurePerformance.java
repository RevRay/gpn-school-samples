package day8;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;



public class MeasurePerformance {
    public static void main(String[] args) {
        List<String> list1 = new LinkedList<>(List.of("a", "b", "c", "d"));
        List<String> list2 = new ArrayList<>(List.of("a", "b", "c", "d"));
//        measureAddPerformance(list1);
//        measureAddPerformance(list2);
//        measureInsertPerformance(list1);
//        measureInsertPerformance(list2);
//
//        measureRemovePerformance(list1);
//        measureRemovePerformance(list2);'

//        Executable e = new Executable() {
//            @Override
//            public void execute(List<String> list, String addedValue) {
//                list.add(3, addedValue);
//            }
//        };
//        Executable e = (list, addedValue) -> list.add(3, addedValue);

        Executable e = new Executable() {
            @Override
            public void execute(List<String> list, String addedValue) {

            }
        };

        measurePerformance(list1, e, 50000);
        measurePerformance(list2, e, 50000);
    }

//    public static void measureAddPerformance(List<String> list){
//        System.out.println(list.getClass());
//        long timeBefore = System.currentTimeMillis();
//        for (int i = 0; i < 100000; i++) {
//            list.add("1000" + i);
//        }
//        long timeAfter = System.currentTimeMillis();
//
//        System.out.println("before: " + timeBefore);
//        System.out.println("after: " + timeAfter);
//        System.out.println("total time:" + (timeAfter - timeBefore));
//    }
//
//    public static void measureInsertPerformance(List<String> list){
//        System.out.println(list.getClass());
//        long timeBefore = System.currentTimeMillis();
//        for (int i = 0; i < 100000; i++) {
//            list.add(3, "1000" + i);
//        }
//        long timeAfter = System.currentTimeMillis();
//
//        System.out.println("before: " + timeBefore);
//        System.out.println("after: " + timeAfter);
//        System.out.println("total time:" + (timeAfter - timeBefore));
//    }
//
//    public static void measureRemovePerformance(List<String> list){
//        System.out.println(list.getClass());
//        long timeBefore = System.currentTimeMillis();
//        for (int i = 0; i < 100000; i++) {
//            list.remove(3);
//        }
//        long timeAfter = System.currentTimeMillis();
//
//        System.out.println("before: " + timeBefore);
//        System.out.println("after: " + timeAfter);
//        System.out.println("total time:" + (timeAfter - timeBefore));
//    }

    public static void measurePerformance(List<String> list, Executable executable, int iterations){
        System.out.println(list.getClass());
        long timeBefore = System.currentTimeMillis();
        for (int i = 0; i < iterations; i++) {
            executable.execute(list, "asdaf" + i);
        }
        long timeAfter = System.currentTimeMillis();

        System.out.println("before: " + timeBefore);
        System.out.println("after: " + timeAfter);
        System.out.println("total time:" + (timeAfter - timeBefore));
    }


}
