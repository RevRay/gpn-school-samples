package day8;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class MapDemo {
    public static void main(String[] args) {
        Map<String, String> userData = new HashMap<>();
        //key - String value - String

        userData.put("d5f@ya.ru", "Kuzmichev");
        userData.put("344@ya.ru", "Ivanov");
        userData.put("mail@ya.ru", "Petrov");
        userData.put("imbox@ya.ru", "Sidorov");
        userData.put(null, "Default name");



//        System.out.println(userData);
//
//        String userName = userData.get("mail@ya.ru");
//        String userName1 = userData.get(null);
//        System.out.println(userName);
//        System.out.println(userName1);

        for (Map.Entry<String, String> e : userData.entrySet() ) {
            System.out.println("Map element: " + e);
        }

        System.out.println(userData.values());
        System.out.println(userData.keySet());

        System.out.println(userData.get("amazing@email.ru"));

        Apple poisonousApple = new Apple(13, "crimson red");

        Map<Apple, Integer> applePrices = new HashMap<>();
        applePrices.put(new Apple(130, "green"), 900);
        applePrices.put(new Apple(122, "red"), 906);
        applePrices.put(new Apple(144, "bue"), 920);
        applePrices.put(new Apple(15, "singapur"), 400);
//        applePrices.put(new Apple(13, "crimson red"), 400);
        applePrices.put(poisonousApple, 933);


//        poisonousApple.weight = 129;


        System.out.println(applePrices);
        System.out.println(applePrices.get(new Apple(13, "crimson red")));

        //get   Object
        //1. hash //Override
        //2. equals


        Map<Box, String> map = new TreeMap<Box, String>();
        map.put(new Box(10), null);
        System.out.println(map);

    }
}
