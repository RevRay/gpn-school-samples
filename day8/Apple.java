package day8;

import java.util.Objects;

public class Apple implements Comparable<Apple> {
    public final int weight;
    public final String sort;


    double id = Math.random();

    public Apple(int weight, String sort) {
        this.weight = weight;
        this.sort = sort;
    }

    @Override
    public String toString() {
        return "Apple{" +
                "weight=" + weight +
                ", sort='" + sort + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Apple apple = (Apple) o;
        return weight == apple.weight && Objects.equals(sort, apple.sort);
    }

    @Override
    public int hashCode() {
        return Objects.hash(weight, sort);
//        return 0;
    }

    @Override
    public int compareTo(Apple o) {
//        if (this.weight > o.weight) {
//            return 1; +
//        } else if (this.weight < o.weight) {
//            return -1; -
//        } else {
//            return 0;
//        }
        return this.weight - o.weight;
    }

}
