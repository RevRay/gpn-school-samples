package day8;

import java.util.Comparator;

public class SortLengthAppleComparator implements Comparator<Apple> {
    @Override
    public int compare(Apple o1, Apple o2) {
        return o1.sort.length() - o2.sort.length();
    }
}
