package day8;

import day7.Stack;

public class BreakingStack {
    public static void main(String[] args) {
        Stack<String> s = new Stack<String>(10);
        s.push("");
        s.push("");
        s.push("");
        s.push("");
        s.push("");

//        s.stackTop = -1;
        s.getElements(); //private field + getter & setter
        s.pop();
    }
}
