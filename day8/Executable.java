package day8;

import java.util.List;

@FunctionalInterface
public interface Executable {
    void execute(List<String> list, String addedValue);
//    void execute2(List<String> list, String addedValue);
}
