package day4;

public class Fruit {

    static int countFruitCreated = 0;

    String name;
    int weight;
    String color;

    public Fruit(String name, int weight, String color) {
        this.name = name;
        this.weight = weight;
        this.color = color;
        countFruitCreated++;
    }

    public String toString() {
        return String.format("Fruit name: %s, weight: %d, color: %s", name, weight, color);
    }
}
