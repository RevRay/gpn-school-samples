package day4.paradigms;

public class House extends Object {
    int price;
    int floorCount;

    public void build() {
        System.out.println(String.format("House built! Price = %d, floor count = %d", price, floorCount));
    }

    public void repair() {
        System.out.println("House repaired!");
    }
}
