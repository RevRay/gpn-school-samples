package day4.paradigms;

public class A {

    public int i = 90;
    protected int i2 = 9666; //текущий пакет + классы наследники
    int i3 = 88; //<default> //текущий пакет
    private int i4 = 13;

    public A (int i, String s) {
        //super(); //Object
        System.out.println("created A object");
        System.out.println(i4);
    }

//    public A(){}
}
