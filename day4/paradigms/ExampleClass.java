package day4.paradigms;

public class ExampleClass {
    public ExampleClass(int i) {
        System.out.println(i);
    }

    public ExampleClass() {
        System.out.println("using no args constructor");
    }
}
