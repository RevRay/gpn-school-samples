package day4.paradigms;

public class Cottage extends House{

    boolean hasSaray;

    public void makeWarmer() {
        System.out.println("House made warmer!");
    }

    public Cottage(int price, int floorCount, boolean hasSaray) {
        this.price = price;
        this.floorCount = floorCount;
        this.hasSaray = hasSaray;
    }

    @Override
    public void build() {
        System.out.println("Cottage built successfully");
    }
}
