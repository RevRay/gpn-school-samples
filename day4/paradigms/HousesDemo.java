package day4.paradigms;

public class HousesDemo {
    public static void main(String[] args) {

        System.out.println("");
        System.out.println(123);

        Math.max(1,3);
        Math.max(1L, 3L);
        Math.max(1.0,3.0);

        Cottage cottage1 = new Cottage(1, 2, true);

        Cottage cottage = new Cottage(2, 2, true);
        House h1 = cottage;
        Object o = cottage;

        ((Cottage) h1).makeWarmer();

        //Cottage IS A House

        Skyscraper sc = new Skyscraper(1,1,1,1);

        //Cottage IS NOT a Skyscraper
//        ((Cottage) sc).hasSaray

        cottage.build();
        cottage.repair();

        Skyscraper skyscraper = new Skyscraper(3, 50, 4, 7);
        skyscraper.build();

        House house = new Skyscraper(3, 50, 4,5);
        house.repair();
        house = new Cottage(3,4,false);
        house.repair();
        house.build();
        house = new House();
        house.repair();










    }
}
