package day4.paradigms.oop;

public class OOPApproachDemo {

    public static void main(String[] args) {

        Trader[] traders = {
                new Trader("Charlie", 20, 9.0),
                new Trader("Ivan", 28500, 4.3),
                new Trader("Dylan", 9, 1501.7)
        };

        for (Trader t : traders) {
            System.out.println(t.calculateAllAssetsPrice());
        }



        Trader trader1 = new Trader("Charlie", 20, 9.0);
        Trader trader2 = new Trader("Ivan", 28500, 4.3);
        Trader trader3 = new Trader("Dylan", 9, 1501.7);

        System.out.println(trader1.calculateAllAssetsPrice());
        System.out.println(trader2.calculateAllAssetsPrice());
        System.out.println(trader3.calculateAllAssetsPrice());
    }

}
