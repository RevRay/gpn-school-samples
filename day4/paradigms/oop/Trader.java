package day4.paradigms.oop;

public class Trader {

    String traderName; //null
    int sharesCount; //0
    double averageSharePrice; //0.0


    public Trader(String traderName, int sharesCount, double averageSharePrice) {
        this.traderName = traderName;
        this.sharesCount = sharesCount;
        this.averageSharePrice = averageSharePrice;
    }

    public double calculateAllAssetsPrice() {
        return sharesCount * averageSharePrice;
    }

}
