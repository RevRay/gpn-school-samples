package day4.paradigms;

/*
объект трейдер
- имя
- количество акций
- средняя цена акции

Задача - вывести суммарную стоимость портфеля
 */
public class ProceduralApproachDemo {

    public static void main(String[] args) {

        String[] names = {"Charlie", "Ivan", "Dylan"};
        int[] traderSharesCount = {20, 28500, 9};
        double[] averageSharePrices = {3.0, 2.3, 10.0};

//        String traderName1 = "Charlie";
//        int sharesCount1 = 20;
//        double averageSharePrice1 = 9.0;
//
//        String traderName2 = "Ivan";
//        int sharesCount2 = 28500;
//        double averageSharePrice2 = 4.3;
//
//        String traderName3 = "Dylan";
//        int sharesCount3 = 9;
//        double averageSharePrice3 = 1501.7;

        for (int i = 0; i < names.length; i++) {
            printAssetPrice(traderSharesCount[i], averageSharePrices[i]);
        }

//        printAssetPrice(sharesCount1, averageSharePrice1);
//        printAssetPrice(sharesCount2, averageSharePrice2);
//        printAssetPrice(sharesCount3, averageSharePrice3);

    }

    public static void printAssetPrice(int sharesCount, double averageSharePrice) {
        double totalAssetPrice1 = sharesCount * averageSharePrice;
        System.out.println(totalAssetPrice1);
    }

}
