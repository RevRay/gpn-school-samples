package day4.paradigms;

public class Skyscraper extends House {

    int elevatorCount;
    int entranceCount;

    public Skyscraper(int price, int floorCount, int elevatorCount, int entranceCount) {
        this.price = price;
        this.floorCount = floorCount;
        this.elevatorCount = elevatorCount;
        this.entranceCount = entranceCount;
    }

    public void rentCommercial() {
        System.out.println("started renting");
    }
}
