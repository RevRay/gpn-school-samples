package day4.paradigms;

public class WoodenCottage extends Cottage {

    public WoodenCottage(int price, int floorCount, boolean hasSaray) {
        super(price, floorCount, hasSaray);
    }
}
