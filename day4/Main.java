package day4;

import day5.DayOfWeek;

public class Main {
    public static void main(String[] args) {
        Fruit f = new Fruit("apple", 23, "green");
        Fruit f2 = new Fruit("pineapple", 235, "light green");
        Fruit f3 = new Fruit("pineapple", 235, "light green");
        Fruit f4 = new Fruit("pineapple", 235, "light green");

//        System.out.println(f.color);
//        System.out.println(f.name);
//        System.out.println(f.weight);

        //Object

        System.out.println(f);
        System.out.println(f2);

        System.out.println(Fruit.countFruitCreated);
    }
}
