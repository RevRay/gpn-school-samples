package day5.datastructures;

public class StackClient {
    public static void main(String[] args) {
        Stack s = new Stack(10);

        s.push(40);
        System.out.println(s);
        s.push(45);
        System.out.println(s);

        s.pop();
        System.out.println(s);
        s.pop();
        System.out.println(s);

        s.push(66);
        System.out.println(s);
        s.push(77);
        System.out.println(s);


        int[] array = new int[10];
        array[0] = 40;
        array[1] = 40;
        array[2] = 55;
    }
}
