package day5.datastructures;

import java.util.Arrays;

//стек целых чисел
// 12 0 3 43 155
// 0  0 0  0   0  0  0  0  0  0
public class Stack {

    int[] elements; //10 = 20
    int stackTop;

    public Stack(int stackSize) {
        this.elements = new int[stackSize];
        this.stackTop = -1;
    }

    //gets values
    public int pop() {
        if (stackTop == -1) {
            System.out.println("Stack is empty!");
        }
        return elements[stackTop--];
    }

    public void push(int newElement) {
        if (stackTop >= elements.length - 1) {
            System.out.println("Stack size exceeded");
            return;
        }
        elements[++stackTop] = newElement;
    }

    @Override
    public String toString() {
        return "Stack{" +
                "elements=" + Arrays.toString(elements) +
                ", stackTop=" + stackTop +
                '}';
    }
}
