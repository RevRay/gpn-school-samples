package day5;

public enum DayOfWeek {
    MONDAY("Monday", "Понедельник"), //Monday   DayOfWeek()
    TUESDAY("Tuesday", "Вторник"),
    WEDNESDAY("Wednesday", "");

    DayOfWeek(String alias, String ruAlias) {
        this.alias = alias;
        this.ruAlias = ruAlias;
    }

    public final String alias;
    public final String ruAlias;
}
