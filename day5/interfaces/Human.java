package day5.interfaces;

public class Human extends Animal implements Chargeable{

    @Override
    public void move() {
        System.out.println("human moves");
    }

    @Override
    public int charge() {
        return 0;
    }
}
