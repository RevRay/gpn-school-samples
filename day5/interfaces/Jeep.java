package day5.interfaces;

public class Jeep implements Moveable, Chargeable{

    String color;

    @Override
    public void move() {
        System.out.println("jepp moves");
    }

    @Override
    public int charge() {
        return 0;
    }
}
