package day5.interfaces;

public class Launcher {
    public static void main(String[] args) {
        Jeep j = new Jeep();
        Moveable m = j;
        Chargeable c = j;
        m.move();

        Moveable[] mArr = {new Human(), new Jeep()};
        for (Moveable m1 : mArr) {

        }

    }
}
