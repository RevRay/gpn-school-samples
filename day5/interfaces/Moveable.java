package day5.interfaces;

//определение действий, что умеет объект
//не может хранить состояние объекта
public interface Moveable {
    boolean mammal = true; //static

    void move(); //public abstract
}
