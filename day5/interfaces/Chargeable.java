package day5.interfaces;

public interface Chargeable {
    int charge();
}
