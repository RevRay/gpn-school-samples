package day5.multipleinheritance;

public class Vehicle {

    int wheelsCount;
    String musicPlayerType;

    public final void drive() {
        System.out.println("Driving...");
    }

}
