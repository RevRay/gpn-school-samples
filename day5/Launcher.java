package day5;

public class Launcher {
    public static void main(String[] args) {
        String[] daysOfWeekStrings = {"Tuesday", "Wednesday"};
        DayOfWeek dayOfWeek = DayOfWeek.TUESDAY;

//        if (dayOfWeek == DayOfWeek.TUESDAY) {
//            System.out.println("no classes Java AT");
//        } else if (dayOfWeek == DayOfWeek.WEDNESDAY) {
//            System.out.println("we have classes");
//        }

//        switch (dayOfWeek) {
//            case TUESDAY:
//                System.out.println("no classes Java AT");
//                System.out.println(dayOfWeek);
//                break;
//            case WEDNESDAY:
//                System.out.println("we have classes");
//        }

        DayOfWeek[] days = DayOfWeek.values();
        for (DayOfWeek d : days) {
            System.out.println(d + " " + d.alias + " " + d.ruAlias);
        }

        String s = "TUESDAY";
        DayOfWeek day = DayOfWeek.valueOf(s);

//        day.alias = "ovember";
        System.out.println(day + " " + day.alias + " " + day.ruAlias);

//        System.out.println(day);
//
//        int index = day.ordinal();
//        System.out.println(DayOfWeek.values()[index]);


    }
}
