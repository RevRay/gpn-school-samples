package day5.abstractclasses;

public interface Sellable {

    int icoursive = 10; //public static

//    int getPrice(); //abstract - 7 Java
    default int getPrice() { //public
        return 0;
    }

}
