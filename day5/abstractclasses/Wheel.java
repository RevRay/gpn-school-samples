package day5.abstractclasses;

public class Wheel implements Sellable{
    int price = 90;

    @Override
    public int getPrice() {
        return price;
    }
}
