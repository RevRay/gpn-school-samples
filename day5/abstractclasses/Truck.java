package day5.abstractclasses;

public class Truck extends Auto implements Sellable{
    int mileage;
    int basePrice;

    public Truck(int mileage) {
        this.mileage = mileage;
        this.basePrice = 6_000_000;
    }

    @Override
    public void drive() {

    }

    @Override
    public int getPrice() {
        return this.basePrice - mileage * 3;
    }
}
