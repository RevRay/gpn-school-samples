package day5.abstractclasses.phones;

public interface AnotherAbstractPhone {
    void dial();
    void hangUp();
    void turnOn();
    void turnOff();
}
