package day5.abstractclasses.phones;

public class AndroidPhone extends AbstractPhone {

    public AndroidPhone() {
        super();
    }

    @Override
    public void dial() {

    }

    @Override
    public void hangUp() {

    }

    @Override
    public void turnOn() {
        System.out.println("");
    }

    @Override
    public void turnOff() {

    }
}
