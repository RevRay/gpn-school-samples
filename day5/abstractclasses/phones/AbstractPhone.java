package day5.abstractclasses.phones;

public abstract class AbstractPhone {

//    public AbstractPhone(){
//        super();
//        System.out.println("abstract phone created");
//    }

    public abstract void dial();
    public abstract void hangUp();
    public abstract void turnOn();
    public abstract void turnOff();
}
