package day5.abstractclasses;

public class SUV extends Auto implements Sellable{
    int previousOwners;
    int basePrice;

    public SUV(int previousOwners) {
        this.previousOwners = previousOwners;
        this.basePrice = 800000;
    }

    @Override
    public void drive() {
        System.out.println("suv driving");
    }

    @Override
    public int getPrice() {
        return basePrice - previousOwners * 10000;
    }
}
