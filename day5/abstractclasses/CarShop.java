package day5.abstractclasses;

public class CarShop {
    public static void main(String[] args) {
        SUV suv = new SUV(5); //
        Truck truck = new Truck(400000);
        Wheel wheel = new Wheel();


        Sellable[] goods = {suv, truck, wheel, new Wheel(), new Truck(50000)};

        int assetsPrice = 0;
        for (Sellable s : goods) {
            assetsPrice = assetsPrice + s.getPrice();
        }

        System.out.println(assetsPrice);
    }
}
