package day5.abstractclasses;

public abstract class Auto {

    int wheelsCount;
    String engineType;
    int maxSpeed;

//    public void drive() {
//        System.out.println("driving...");
//    }

    public abstract void drive();

}

