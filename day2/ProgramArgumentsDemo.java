package day2;

import java.util.Arrays;
import java.util.Random;

/**
 * Пример того, как мы можем менять поведение программы через передачу аргументов командной строки.
 *
 * Для того чтобы в Intellij IDEA указать аргументы командной строки
 * откройте в правом верхнем углу окно конфигурации запуска и выберите пункт Edit Configurations.
 * В секции Build and run найдите текстовое поле, подписанное Program arguments и введите через пробел
 * список передаваемых параметров. Переданные таким образом параметры окажутся доступны в методе main
 * в переменной String[] args.
 *
 * В примере ниже наше приложения имитирует бросок кубика. Но в зависимости от первого
 * переданного аргумента (args[0]) выполняется бросок кубика с указанном в аргменте количеством граней.
 */
public class ProgramArgumentsDemo {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(args));

        int diceSides = Integer.parseInt(args[0]); //"6" -> 6;  "abc" -> exception

        Random rnd = new Random();
        int diceRoll = rnd.nextInt(diceSides) + 1;
        System.out.println("Throw dice: " + diceRoll);
    }
}
