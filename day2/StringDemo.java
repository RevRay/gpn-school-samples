package day2;

import java.util.Arrays;

public class StringDemo {

    /**
     * String, т.е. строка - это последовательность символов.
     * @param args
     */
    public static void main(String[] args) {
        //Есть несколько вариантов создания новой строки
        //1. через строковый литерал (на начальном этапе - предпочтительный вариант)
        String s1 = "I am a string";
        //2. через вызов конструктора и создание строки
        String s2 = new String("test string");
        //3. через вызов конструктора и создание строки на базе char[]
        char[] chars = {'a', ' ', 'w', 'o', 'r', 'd'};
        String s3 = new String(chars);

        //ОСНОВНЫЕ МЕТОДЫ СТРОК
        //получение длины строки:
        String s4 = "How long am I?";
        int strLength = s4.length();
        System.out.println("Длина строки '" + s4 + "' - " + s4.length() + "символов");

        //проверить, содержится ли в строке нужная нам подстрока (слово, последовательность символов)
        String ticket1 = "[x] Alex 4D 345";
        String ticket2 = "[ ] Alex 5D 347";
        System.out.println(ticket1.contains("123"));

        //получение подстроки (нужной части исходной строки)
        System.out.println(ticket1.substring(0, 4));
        System.out.println(ticket1.substring(4));

        //получения символа на конкретной позиции строки
        String s5 = "abcdef";
        System.out.println(s2.charAt(5));
    }

}
