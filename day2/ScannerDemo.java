package day2;

import java.util.Scanner;

public class ScannerDemo {
    /**
     * В этом примере демонстрируется работа класса Scanner - способ считывать команды пользователя из консоли.
     *
     * @param args
     */
    public static void main(String[] args) {
        //Создаем объект сканнера
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter your name:"); //спрашиваем пользователя о его имени
        String userName = sc.nextLine(); //ожидаем ввод в консоль и сохраняем в переменную все что ввел пользователь

        System.out.println("Enter your last name:"); //спрашиваем пользователя о его фамилии
        String userLastName = sc.nextLine(); //ожидаем ввод в консоль и сохраняем в переменную все что ввел пользователь

        //проверяем, что пользователь ввел и имя и фамилию
        if (userName.length() > 0 && userLastName.length() > 0) { //при выполнении условия - приветствуем пользователя
            System.out.println(String.format("Hello! Your name is: %s, your last name is: %s", userName, userLastName));
        } else { //если или фамилия или имя пустые - предупреждаем об этом пользователя
            System.out.println("You have not entered your name or last name");
        }
    }
}
