package day3;

import day5.YearSeasons;

public class Hometask3 {
    public static void main(String[] args) {
        String[] monthNames = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        int[] monthLengths = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        String[] daysOfWeek = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

        for (int monthIndex = 0; monthIndex < monthNames.length; monthIndex++) { //перебираем каждый месяц
            for (int i = 0; i < monthLengths[monthIndex]; i++) { //цикл по дням
//                System.out.println(monthIndex % 3 == 1 ? YearSeasons.);
                System.out.println(String.format("%s - %s - %d%s", monthNames[monthIndex], daysOfWeek[i % 7], i + 1, i % 7 > 4 ? " - Holiday!" : ""));
            }
        }
    }
}
