package day3;

import java.util.Arrays;

/**
 * Здесь приводятся алгоритм сортировки и два поисковых алгоритма - пример использования циклов и условий.
 */
public class SortAndSearchDemo {

    public static void main(String[] args) {
        String[] strs = {"Alex", "Bob", "Margareth", "Lalo", "Ben", "", "Yen", "Kyle", "", "George"};
        String stringThatWeSearchFor = "George";

        int[] ints = {8, -9, 0, 16, 11, 2, 67, 45, 1, 0, 53, -4, -2};
        System.out.println(Arrays.toString(ints));
        sortArrayWithBubbleSort(ints);
        System.out.println(Arrays.toString(ints));

        findElementWithBinarySearch(ints, 53);
    }

    /**
     * Binary search - Бинарный поиск
     * Может работать только с предварительно отсортированным массивом
     * Более производительный чем линейный поиск
     * @param array
     * @param searchedNumber
     * @return
     */
    public static int findElementWithBinarySearch(int[] array, int searchedNumber) {
        int lowerBound = 0;
        int upperBound = array.length - 1;
        int index;

        while (true) {
            index = lowerBound + (upperBound - lowerBound) / 2;
            int currentValue = array[index]; //на занятии нам не хватало этой строчки
            System.out.println(String.format("Нижняя и верхняя границы диапазона поиска: [%d..%d]", lowerBound, upperBound));

            if (currentValue > searchedNumber) {
                //значит нужно искать в левой части массива
                upperBound = index - 1;
            } else if (currentValue < searchedNumber) {
                //значит нужно искать в правой части массива
                lowerBound = index + 1;
            } else {
                System.out.println(String.format("Искомое число '%d' найдено по индексу '%d', значение - '%d'", searchedNumber, index, currentValue));
                return index;
            }
        }
    }

    /**
     * Bubble sort - сортировка пузырьком
     * @param array
     */
    public static void sortArrayWithBubbleSort(int[] array) {
        //цикл для выбора массива поиска
        for (int i = 1; i < array.length; i++) {
            //локальный цикл сортировки массива
            for (int j = 0; j < array.length - 1; j++) { // i = array.length - 1
                if (array[j] > array[j + 1]) {
                    swapElements(array, j, j + 1);
                    System.out.println(Arrays.toString(array));
                }
            }
        }
    }

    public static void swapElements(int[] array, int i, int j) {
        int temp = array[i]; //текущий элемент слева
        array[i] = array[j];
        array[j] = temp;
    }

    /**
     * Linear search - Линейный поиск
     * Может работать и с отсортированным и с неотсортированным массивом
     * Медленнее бинарного поиска
     * @param array
     * @param searchedElement
     * @return
     */
    public static int findElementWithLinearSearch(String[] array, String searchedElement) {
        int indexOfElement = -1;

        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(searchedElement)) {
                System.out.println(String.format("'%s' word was found at index %d", searchedElement, i));
                System.out.println("Element search took" + i);
                indexOfElement = i;
                return indexOfElement;
            }
        }

        return indexOfElement;
    }


}
