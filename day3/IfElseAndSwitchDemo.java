package day3;

public class IfElseAndSwitchDemo {
    public static void main(String[] args) {

        String weather = new String("still");//, "rain" "storm" "still"
        System.out.println(weather);


//        if (weather.equals("sunny")) {
//            System.out.println("it time to go for a walk");
//        } else if (weather.equals("rain")) {
//            System.out.println("its better to stay home");
//        } else if (weather.equals("still")) {
//            System.out.println("it OK");
//        } else if (weather.equals("storm")) {
//            System.out.println("hold yourself");
//        } else {
//            System.out.println("weather unknnown");
//        }

        //byte short int String Enum
        switch (weather) {
            case "sunny":
                System.out.println("it time to go for a walk");
                break;
            case "storm":
                System.out.println("its better to stay home");
                break;
            case "still":
                System.out.println("it OK");
                break;
            case "rain":
                System.out.println("hold yourself");
                break;
            default:
                System.out.println("weather unknnown");
                break;
        }


    }
}
