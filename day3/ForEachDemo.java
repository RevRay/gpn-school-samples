package day3;

public class ForEachDemo {

    public static void main(String[] args) {
        int[] myArray = {5, 3, 7, 8};
        int i = 9; //[9]

        printNumbers(myArray);
        printNumbers(i);
    }

    public static void printNumbers(int... array){ //int[] int...
        for (int i : array) {
            System.out.println(i);
        }
    }

}
