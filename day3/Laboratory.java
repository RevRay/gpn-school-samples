package day3;

import java.util.Arrays;

/**
 * Молодой химик-энтузиаст получил доступ к лаборатории химфака.
 * Войдя в помещение, он видит помимо всякой заумной аппаратуры стеллажи с набором различных веществ в пробирках.
 * В каждой из пробирок – вещество, состоящее из нескольких химических элементов. (String[][])
 *
 * Для своих экспериментов химику необходимы соединения, в состав которых входит платина [Pt],
 * поэтому он хочет понять, какие из пробирок ему пригодятся.
 * Разработайте для него анализатор, который просканирует все пробирки,
 * и выведет на экран только те, которые содержат в своем составе платину.
 *
 * Пример входных данных:
 * {"H", "H", "O"}, //vial 1
 * {"Ag", "U", "He"}, //vial 2
 * {"O", "Pt", "C", "O"}, //vial 3
 * {"Br", "Ba"} //vial 4
 */
public class Laboratory {
    public static void main(String[] args) {

        String elementThatWeSearch = "Pt";

        String[][] storage = {
                {"O", "Pa"}, //1
                {"Pt", "H", "Au", "F"}, //2
                {"He", "Ar"}, //3
                {"He", "Pt", "O", "F"} //3
        };

        vial: for (int i = 0; i < storage.length; i++) { //цикл по пробиркам
            elements: for (int j = 0; j < storage[i].length; j++) { //цикл по элементам конкретной пробирки

                if (storage[i][j].equals(elementThatWeSearch)) {
                    System.out.println("Найдена пробирка с платиной! " + Arrays.toString(storage[i]));
                    continue vial;
                }
                System.out.print(storage[i][j] + " ");
            }
            System.out.println();
        }
    }

}
