package day3;

/**
 * Считаем средний рейтинг фильма на основании его оценок
 */
public class KinopoiskAverageScore {

    public static void main(String[] args) {
        int[] filmScores = {10, 8, 7, 2, 5, 8, 10};
                        //   0  1  2  3  4  5

        int scoreSum = 0;
        for (int i = 0; i < filmScores.length; i++) {
            scoreSum += filmScores[i];
        }
        System.out.println("Средний рейтинг фильма: " + 1.0 * scoreSum / filmScores.length);

    }
}
