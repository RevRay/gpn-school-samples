package day1;

public class Day1 {

    public static void main(String[] args) {

        //ПРИМИТИВНЫЕ ТИПЫ ДАННЫХ (8 типов)
        // логический тип данных
        boolean bool = true; // true / false
        // целочисленные типы данных
        byte b = 120; // -128..127
        short s = 2007; // -32768..32767
        char c = 45000; // 0..65535
        int i = 126000; // -2_147_483_648..2_147_483_647
        long l = 890000000000000L; // -9223372036854775808..9223372036854775807
        // дробные типы данных
        float f = 3.0001f;
        double d = 16.09232400744127593057409577640;



        //ОСОБЕННОСТИ ПРЕОБРАЗОВАНИЯ ТИПОВ И АРИФМЕТИЧЕСКИХ ОПЕРАЦИЙ
        //значение меньшего типа можно записать в переменную большего типа
        byte iAmSmall = 6;
        int iAmBig = iAmSmall;
        //но не наоборот
        //iAmSmall = iAmBig; //пытаемся положить большее в меньшее - приведет к ошибке

        //использование любого арифметического оператора, типа: + - / * % и т.д.
        //приводит к тому, что результат меньшего типа возвышается до типа int
        byte bb1 = 1;
        byte bb2 = 2;
        //поэтому без явного преобразования типа следующий код не будет работать:
        //byte bb3 = bb1 + bb2; // так как в нем результат сложения двух byte будет возвышен до int
        //поэтому используем явное преобразование результата к типа byte:
        byte bb3 = (byte) (bb1 + bb2); //+ - / * %

        //деление целочисленных типов будет давать целочисленный результат, без остатка и дробей:
        int i1 = 90;
        int i2 = 40;
        System.out.println(i1 / i2); //выведет в консоль 2
        //для получения остатка используйте оператор %
        System.out.println(i1 % i2); //выведет в консоль 10 (90 / 40 = 2, остаток 10)



        //СИМВОЛНЫЙ ТИП CHAR
        //прямая запись символа
        char c1 = 'G';
        //экранированная запись для специальных символов
        char c2 = '\t';
        char c3 = '\\';
        char c4 = '\'';
        //запись порядкового номера символа в таблице юникод
        char c5 = '\u0089';
        char c6 = 60;
        char c7 = 0x22;

        System.out.println(c6); //вместо числа 60 будет выведен символ с порядковым номером 60 из таблицы Unicode



        //ЛОГИЧЕСКИЙ ТИП BOOLEAN
        //сравнение чисел - == (равенство), > (больше), < (меньше)
        int num1 = 90;
        int num2 = 193;
        int num3 = 90;

        System.out.println("num1 == num2: " + (num1 == num2));
        System.out.println("num1 == num3: " + (num1 == num3));
        System.out.println("num1 > num3: " + (num1 > num3));
        System.out.println("num1 < num2: " + (num1 < num3));

        //логические операторы
        boolean isRocketFuelAvailable = true;
        boolean isGermeteized = false;
        //логическое умножение & - возвращает true, только когда оба условия верны (ture)
        boolean isFlightReady = isRocketFuelAvailable & isGermeteized;
        System.out.println("Space launch ready:" + isFlightReady);
        //логическое сложение | - возвращает true, если хотя бы ОДНО из условий верно (true)
        boolean isFlightPartiallyReady = isRocketFuelAvailable | isGermeteized;
        System.out.println("Space launch partially ready:" + isFlightPartiallyReady);



        //ВСПОМОГАТЕЛЬНЫЙ КЛАСС MATH
        int max = Math.max(1, 2); //вернет значение 2 (большее из двух)
        int min = Math.min(-13, 4); //вернет значение -13 (меньшее из двух)
        double d5 = Math.random();
        System.out.println("Вывожу случайно сгенерированное число (дробное значение в диапазоне от 0 до 1): " + d5);



        //СОЗДАНИЕ И ИСПОЛЬЗОВАНИЕ МЕТОДОВ
        //метод - именованный набор команд
        //может быть простым, может иметь список принимаемых параметров,
        //может иметь возвращаемы тип (продукт работы метода), может иметь все сразу

        //пример простого метода:
        printHelloWorld();

        //пример метода с возвращаемым типом:
        double pi = getPiNumber();

        //пример метода с принимаемыми параметрами:
        greetUser("Vasiliy");

        //пример метода с принимаемыми параметрами и с возвращаемым типом:
        int sumResult = add(i1, i2);
    }

    //метод не принимает в себя параметры - пустые круглые скобки ()
    //метод не возвращает из себя никакого значения - указан тип void - "пустота"
    public static void printHelloWorld() {
        String str = "Hello world!";
        System.out.println(str);
    }

    //возвращаемое из метода значения - типа double, возвращается с помощью слова return
    public static double getPiNumber() {
        double pi = 3.14159;
        return pi;
    }

    //принимаемый методом параметр - значение типа String
    //метод не возвращает из себя никакого значения - указан тип void - "пустота"
    public static void greetUser(String userName) {
        System.out.println("Hello, " + userName);
    }

    //принимаемые методом аргументы - два значения типа int
    //возвращаемое из метода значение - int
    public static int add(int i1, int i2) {
        System.out.println("Первый аргумент: " + i1);
        System.out.println("Второй аргумент: " + i2);
        int sum = i1 + 12;
        System.out.println("Полученный результат: " + sum);

        return sum;
    }

}
