package day1;

public class Hometask1 {

    public static void main(String[] args) {
        int num1 = 4;
        int num2 = 5;

        int sumResult = add(num1, num2);
        System.out.println("Результат сложения: " + sumResult);

        int subtractResult = subtract(num1, num2);
        System.out.println("Результат вычитания: " + subtractResult);

        int multiplyResult = multiply(num1, num2);
        System.out.println("Результат умножения: " + multiplyResult);

        int divideResult = divide(num1, num2);
        System.out.println("Результат деления: " + divideResult);

        boolean isNumberEvenResult = isNumberEven(num1);
        System.out.println("Результат проверки на четность: " + isNumberEvenResult);
    }

    public static int add(int a, int b) {
        System.out.println(a);
        System.out.println(b);
        return a + b;
    }

    public static int subtract(int a, int b) {
        System.out.println(a);
        System.out.println(b);
        return a - b;
    }

    public static int multiply(int a, int b) {
        System.out.println(a);
        System.out.println(b);
        return a * b;
    }

    public static int divide(int a, int b) {
        System.out.println(a);
        System.out.println(b);
        return a / b;
    }

    public static boolean isNumberEven(int a) {
        System.out.println("На четность проверяется число: " + a);
        //здесь пользуемся такой логикой - если число четное, то остаток от его деления на 2 будет равен нулю.
        //а если остаток не равен нулю - значит число нечетное.
        //логика выше описывается таким кодом:
        return a % 2 == 0;
    }

}
