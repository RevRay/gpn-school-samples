package day6.test.exceptiondemo;

public class Util {

    public static int parseStringToInt(String str){
        try {
            int parsedValue = Integer.parseInt(str);
            return parsedValue;
        } catch (NumberFormatException e) {
            System.out.println("Parser util received incorrect number");
            throw e;
        }
    }

}
