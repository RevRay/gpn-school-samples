package day6.test.exceptiondemo;

public class ClientApp {
    public static void main(String[] args) {
        String[] array = {"wer", "32", "323dd", "0x023", "==="};

//        if (true) throw new NumberFormatException();

        for (String s : array) {
            int parsedValue = 0;
            try  {
                parsedValue = Util.parseStringToInt(s);
            } catch (NumberFormatException e) {
                System.out.println("Value from array is not a number");
                continue;
            }
//            if (parsedValue == -6) {}
            System.out.printf("Parsed value of %d%n", parsedValue);
        }
    }
}
