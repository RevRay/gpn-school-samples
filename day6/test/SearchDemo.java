package day6.test;

public class SearchDemo {

    public static void main(String[] args) {
        String[] strs = {"dsfsd", "32423", "09090", "sdf", "23"};

        String result = null;
        try {
            result = findString(strs, "00");
        } catch (SearchNotSuccessfulException e) {
            e.printStackTrace();
        }
//        if (result.equals(null)) {
//            System.out.println("no element found");
//            return;
//        }
        System.out.println(result);
        System.out.println(result.length());
    }

    public static String findString(String[] array, String searchQuery) throws SearchNotSuccessfulException {
        for (String element : array) {
            if (element.equals(searchQuery)) {
                return element;
            }
        }
        throw new SearchNotSuccessfulException();
    }

}
