package day6.test.exceptiontypes;

import java.net.MalformedURLException;
import java.net.URL;

public class Demo {
    public static void main(String[] args) {
        String s = null;

        s.length();


        try {
            int i = 9 / 0; //Непроверяемые / Unchecked | RuntimeExceptions - Arithmetic Exception, AIOBE
            int[] array = {34, 34};
//            try {
//                int result = array[2];
//            } catch ()

        } catch (ArithmeticException e) {
            e.printStackTrace();
        }

        //либо try-catch обработка либо указывваем в объявление метода предупреждение
        try {
            URL url = new URL("https://ya.ru"); //Проверяемое исключение - MalformedURLException
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
