package day6.test.generics;

public class BoxClient {
    public static void main(String[] args) {

//        OldBox boxOfNumbers = new OldBox();
//        boxOfNumbers.put(15);
//
//        OldBox boxOfStrings = new OldBox();
//        boxOfStrings.put("string");
//
//        boxOfNumbers.put("dfsfd");
//        boxOfStrings.put(12);
//
//
//        int i = (Integer) boxOfNumbers.get();



        Box<Object> b = new Box<>();
        b.put("");
        b.put(12);

        Box box = new Box();

        box.put("abc");
//        box.put(new RuntimeException());
//        box.put(13); //int -> Integer

        String s = (String) box.get();
        s = (String) box.get();
        s = (String) box.get();
        s = (String) box.get();
        System.out.println(s.length());

        Box<Object> boxOfStrings = new Box<Object>();
        Box<Integer> boxOfIntegers = new Box<>();
        Box<Exception> boxOfexc = new Box<>();

//        boxOfexc.put("sdfsd");

//        int i = boxOfIntegers.get(); //Integer -> int
//        boxOfIntegers.put(12); //int -> Integer
//        String s1 = boxOfStrings.get();
//        boxOfStrings.
//

//        Pair<Integer, String> passport = new Pair<>(2, "");
    }
}
