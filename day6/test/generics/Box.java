package day6.test.generics;

/*
Ведро яблок
Ведро картофеля
Ведро песка
* */
public class Box<T> {

    T o;

    public void put(T o) {
        this.o = o;
    }

    public T get() {
        return o;
    }

}
