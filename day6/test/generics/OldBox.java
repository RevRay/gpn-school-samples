package day6.test.generics;

/*
Ведро яблок
Ведро картофеля
Ведро песка
* */
public class OldBox {

    Object o;

    public void put(Object o) {
        this.o = o;
    }

    public Object get() {
        return o;
    }

}
