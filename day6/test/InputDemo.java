package day6.test;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class InputDemo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String[] names = {"Alex", "Bob", "Charlie"};
        System.out.println(Arrays.toString(names));

        String chosenName = "";

        while (true) {
            int indexOfChosenName = 0;
            try {
                //sc.nextInt()
                indexOfChosenName = Integer.parseInt(sc.nextLine()); //InputMismatchException
                chosenName = names[indexOfChosenName];
//            } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
//                System.out.println("incorrect input");
//            }
            } catch (NumberFormatException e) {
                System.out.println("You have entered letters instead of digits");
                continue;
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Entered incorrect name number");
                continue;
            } finally {
                System.out.println("executed anyway");
            }

            System.out.println(String.format("You picked '%s' name for your hero", chosenName));
            break;
        }


    }
}
