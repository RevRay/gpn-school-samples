package day6.test;

public class ExcpetionsPlayground {
    public static void main(String[] args) {

//        int[] array = {1, 2};
//        int[] array = {1, 2};
//
////        int i = array[7];
//        String s = "assd";
//
//        char c = s.charAt(90); // "0x0e"
//
        testExceptionSyntax();
    }

    public static void testExceptionSyntax() {
        try {
            int division = 9 / 0; //return
        } catch (ArithmeticException e) {

            try {
                int division = 9 / 0; //return
            } catch (ArithmeticException e1) {
                System.out.println("catched exception inside catch");
            }

//            e.printStackTrace();
            System.out.println("test");
        }

        System.out.println("test2");
    }
}
