package day9;

/**
 * интерфейс у которого только один абстрактный метод.
 */
@FunctionalInterface
public interface Drawable {
    String draw(int height, int width); //только один абстр метод
    default void destroy() {
        System.out.println("123");
    }
}
