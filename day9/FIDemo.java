package day9;

import java.util.function.*;

public class FIDemo {
    /**
     * Consumer<T>
     * Supplier<T> - дает нам объекты
     * Predicate<T> - проверяет соответствует ли объект правилу
     * Function<T,R>
     * UnaryOperator<T> //<T,T>
     * @param args
     */
    public static void main(String[] args) {
        Consumer<String> consumer = s -> System.out.println("Consumer consumed object of: " + s);
        consumer.accept("123");

        Supplier<String> randomStringSupplier = () -> {
            double randomValue = Math.random();
            System.out.println("Supplier supplied value of: " + randomValue);
            return "" + randomValue;
        };
        String randomString = randomStringSupplier.get();
        System.out.println(randomString);


        Predicate<String> predicateStringHasEvenNumberOfLetters = new Predicate<String>() {
            @Override
            public boolean test(String s) {
                boolean condition = s.length() % 2 == 0;
                System.out.println("Predicate evaluated to: " + condition);
                return condition;
            }
        };

        predicateStringHasEvenNumberOfLetters.test("123");
        predicateStringHasEvenNumberOfLetters.test("Alex");

        Function<String, Integer> functionToConvertStringToInteger = new Function<String, Integer>() {
            @Override
            public Integer apply(String s) {
                Integer i = Integer.valueOf(s);
                System.out.println("Calculated integer: " + i);
                return null;
            }
        };

        functionToConvertStringToInteger.apply("132");

        UnaryOperator<String> unaryOperator;

    }
}
