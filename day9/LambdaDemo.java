package day9;

public class LambdaDemo {
    public static void main(String[] args) {
        //(переменные) -> действие
//        Drawable d = () -> {
//            int i = 0;
//            System.out.println("drawn!");
//            System.out.println("drawn!");
//            System.out.println("drawn!");
//        };
//
//        d.draw();

//        Drawable d1 = (height, width) -> {};
        Drawable d2 = (height, width) -> "Used pixels to draw object: " + height * width;

        String str = d2.draw(130, 160);
        System.out.println(str);
    }
}
