package day9;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Day9Demo {
    public static void main(String[] args) {
        List<Person> personList = new ArrayList<>();
        personList.add(new Person("Dave", 80));
        personList.add(new Person("John", 14));

        System.out.println(personList);

        List<Person> anotherList = new ArrayList<>(); //shallow copy - поверхностное копирование
        for (Person p : personList) { //deep copy
//            Person copyOfPerson = new Person(p.getName(), p.getAge());
            // Copy constructor
            Person copyOfPerson = new Person(p);

            anotherList.add(copyOfPerson);
        }

        System.out.println(anotherList);


        anotherList.get(0).setAge(75);
        System.out.println(anotherList);
        System.out.println(personList);

        System.out.println(personList == anotherList);


    }
}
