package day9.streamdemos;

import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class PrimitiveStreams {
    public static void main(String[] args) {
        //"%d-ticket"
        //map(Function<T,R>)   int -> String
//        IntStream.of(23, 12, 45, 90, -9, 0, 87).sorted().map(i -> i + "-ticket").forEach(s -> System.out.println(s));
        IntStream sum = IntStream.of(23, 12, 45, 90, -9, 0, 87);
        System.out.println(sum);

//        IntStream.of(0, 1, 2, 3, 4, 5);
        Stream<Integer> st1 = IntStream.range(0, 5).boxed();
        Stream<Integer> st2 = IntStream.rangeClosed(0, 5).boxed();

        Stream.concat(Stream.concat(st1, st2), sum.boxed())
                .forEach(System.out::println);

        IntStream.of(23, 12, 45, 90, -9, 0, 87).skip(2).forEach(s -> System.out.print(s + " "));
        Stream.of("Charlie", "Alex", "Bob", "Bob")
                .sorted()
//                .map(s -> {System.out.println("##" + s); return s;})
                .peek(System.out::println)
                .distinct()
                .forEach(System.out::println);

//        Stream.generate().limit(10);

        Stream<Integer> sum1 = Stream.of(23, 12, 45, 90, -9, 0, 87);
        Consumer<List<Integer>> c  = list -> Collections.sort(list);

    }
}
