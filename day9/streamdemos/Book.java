package day9.streamdemos;

/**
 * название, автор, год публикации, ISBN.
 */
public class Book {
    String name;
    String author;
    int publishYear;
    String isbn;

    public Book(String name, String author, int publishYear, String isbn) {
        this.name = name;
        this.author = author;
        this.publishYear = publishYear;
        this.isbn = isbn;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", publishYear=" + publishYear +
                ", isbn='" + isbn + '\'' +
                '}';
    }
}
