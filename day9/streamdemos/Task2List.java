package day9.streamdemos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Task2List {
    public static void main(String[] args) {
        List<String> colors = new ArrayList<>();
        colors.add("Green");
        colors.add("White");
        colors.add("Black");
        colors.add("Yellow");
        colors.add("Cyan");

        //1 вывести в алфавитном порядке каждый элемент с новоый строки
        //2 для каждого элемента вывести только первую букву

//        Collections.sort(colors);
//        for (String s : colors) {
//            System.out.println(s);
//        }
//
//        for (String s : colors) {
//            char c = s.charAt(0);
//            System.out.println("char is " + c);
//        }



        colors.stream()
                .sorted()
                .map(s -> {System.out.println(s); return s;})
                .forEach(s -> System.out.println("char is " + s.charAt(0)));


    }
}
