package day9.streamdemos;

import java.util.function.Supplier;
import java.util.stream.Stream;

public class StreamDemo {
    public static void main(String[] args) {
//        Stream.of(1, 2, 3, 4, 5).forEach(i -> System.out.println(i));

        //generate ages  count 13
        Stream.generate(() -> (int) (Math.random() * 100)).limit(3).forEach(c -> System.out.println(c));


        Stream.of("aasd", "dfsdf", "ssdf", "asasdf", "arewr")
                .sorted()
                .filter( s -> s.charAt(0) == 'a' )
                .filter( s -> s.length() <= 4 )
                .map(s -> "prefix-" + s)
                .forEach(s -> System.out.println(s));
    }
}
