package day9.streamdemos;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Random;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * Задание 9.1. Генератор человеков
 * <p>
 * Дан строковый массив имен. Для каждого имени вывести в консоль имя + случайно сгенерированный возраст.
 * Написать решение в функциональном стиле (стримы, лямбды, ...)
 */
public class Task1 {
    public static void main(String[] args) {
        String[] names = {
                "Charlie",
                "Steve",
                "Robert",
                "Catherine",
                "Simon",
                "Dave",
                "Jessica"};


        Random rnd = new Random();
//        for (int i = 0; i < names.length; i++) {
//            int randomAge = rnd.nextInt(101) + 1;
////            String randomName = names[rnd.nextInt(names.length)];
//            System.out.println(String.format("person %s is %d years old", names[i], randomAge));
//        }

        Arrays
                .stream(names)
                .forEach(s -> System.out.println(String.format("person %s is %d years old", s, rnd.nextInt(101) + 1)));


    }
}
