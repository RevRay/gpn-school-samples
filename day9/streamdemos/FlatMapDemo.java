package day9.streamdemos;

import java.util.List;

/**
 * Метод flatMap() - "разравнивает", "делает плоской" имеющуюся структуру
 * данных, позволяет избавиться от лишней вложенности.
 * Где-то вглубине себя у нас просто происходит сложение нескольких стримов в один большой стрим элементов.
 */
public class FlatMapDemo {
    public static void main(String[] args) {
        List<List<Integer>> peopleBalances = List.of(
                List.of(45, 56, 90),
                List.of(600, 70, 10),
                List.of(90, 70, 11, 10)
        );

        peopleBalances.stream()
                .flatMap(l -> l.stream()) //в результате преобразования flatMap()
                //мы теперь работаем не со Stream<List<Integer>>, а с List<Integer>,
                //и теперь нам на порядок проще работать с отдельными значениями.
                //просто для интереса можете попробовать написать аналогичное решение,
                //но без reduce() - и увидите соль проблем и неудобств.
                .forEach(System.out::println);
    }
}
