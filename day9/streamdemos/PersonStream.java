package day9.streamdemos;

import day9.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PersonStream {
    public static void main(String[] args) {
        List<Person> persons = new ArrayList<>();
        //18 - 70
        persons.add(new Person("John", 19));
        persons.add(new Person("Joh2n", 15));
        persons.add(new Person("Jo3hn", 80));
        persons.add(new Person("Jo4hn", 27));

//        List<Person> filteredList = persons.stream()
//                .filter(p -> p.getAge() >= 18)  //lazy
//                .filter(p -> p.getAge() <= 70)
////                .toArray();
//                .collect(Collectors.toList());
        long count = persons.stream()
                .filter(p -> p.getAge() >= 18)  //lazy
                .filter(p -> p.getAge() <= 70)
//                .toArray();
                .count();

        System.out.println(count);
//        System.out.println(filteredList);
    }
}
