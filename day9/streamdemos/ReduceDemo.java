package day9.streamdemos;

import day8.Apple;
import java.util.List;

/**
 * Reduce - позволяет уменьшить, "схлопнуть" стрим элементов типа в один элемент этого же типа, т.е.
 * Stream<T> - в T
 * Например - Stream<Integer> - в Integer
 * Stream<String> - в String
 *
 * Метод reduce() принимает в себя BinaryOperator<T> (т.е. BiFunction<T,T,T>)
 * Лямбда для B может выглядеть так:
 * (аккумулятор, элемент) -> операция с аккумулятором и элементом
 *
 * Например, возьмем пример сложения всех чисел стрима:
 * Stream.of(1,4,3).reduce((accumulator, element) -> accumulator + element)
 * В переменную accumulator будут накапливаться значения - результаты сложения, результаты работы правой части лммбды.
 * Поэтапно, это будет выглядеть так:
 * 1. accumulator = 0, element = 1, accumulator + element = 1 (результат записывается в accumulator, переходим к след. элементу)
 * 2. accumulator = 1, element = 4, accumulator + element = 5 (результат записывается в accumulator, переходим к след. элементу)
 * 3. accumulator = 5, element = 3, accumulator + element = 8 (больше элементов нет, возвращаем результат - 8)
 *
 * Ниже в коде приведены еще несколько примеров работы reduce:
 */
public class ReduceDemo {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(1, 4, 2, 8, 7);
        List<String> cities = List.of("Moscow", "Dublin", "San-Francisco", "Novosibirsk", "Astana");
        List<Apple> apples = List.of(
                new Apple(500, "Fudzi"),
                new Apple(200, "Granny Smith"),
                new Apple(500, "Golden")
        );

        reduceCitiesToTravelRoute(cities);
        reduceNumbersToTheirSum(numbers);
        reduceNumbersToTheirProduct(numbers);
        reduceApplesToTotalWeight(apples);
    }

    public static void reduceCitiesToTravelRoute(List<String> cities) {
        String route = cities.stream()
                .reduce((accumulator, element) -> accumulator + " -> " + element).get();
        System.out.println("Travel route is: " + route);
    }

    public static void reduceNumbersToTheirSum(List<Integer> numbers) {
        int sum = numbers.stream()
                .reduce((accumulator, element) -> accumulator + element).get();
        System.out.println("Result of adding all numbers:" + sum);
    }

    public static void reduceNumbersToTheirProduct(List<Integer> numbers) {
        int product = numbers.stream()
                .reduce((accumulator, element) -> accumulator * element).get();
        System.out.println("Result of multiplying all numbers:" + product);
    }

    public static void reduceApplesToTotalWeight(List<Apple> apples) {
        int weight = apples.stream().map(a -> a.weight).reduce((accumulator, element) -> accumulator + element).get();
        System.out.println("All apples weight a total of: " + weight);
    }

}
