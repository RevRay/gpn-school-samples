package day7;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SetDemo {
    public static void main(String[] args) {
        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(77);
        set.add(5);
        set.add(4);
        set.add(1);

        System.out.println(set);
        //set.get(0); у Set-ов нет операции get, обратиться к отдельному элементу мы не можем
        //но если это нужно - создаем список на базе сета. список слава богу умеет делать get
        List<Integer> list = new ArrayList<>(set);
        System.out.println(list.get(2));

        list.add(1);
        list.add(8);
        list.add(1);
        list.add(7);
        list.add(1);

        System.out.println(list);
    }
}
