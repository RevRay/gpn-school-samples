package day7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ListCreationDemo {
    public static void main(String[] args) {
        List<Integer> list = new LinkedList<>();
        list.add(60);
        list.add(50);
        list.add(50);
        list.add(51);

        list.get(3);
        //List  ArrayList  LinkedList

        List<Integer> list2 = new ArrayList<>(Arrays.asList(60, 50, 50, 51));

        List<Integer> list3 = Arrays.asList(60, 50, 50 ,51); //imutable list - elements can be modified

        List<Integer> list4 = List.of(60, 50, 50, 51); //immutable list - elements can not be modified
        list4.set(2, 99);


        System.out.println(list.getClass());
        System.out.println(list2.getClass());
        System.out.println(list3.getClass());
        System.out.println(list4.getClass());

        modifyList(list);



    }

    public static void modifyList(List<Integer> list) {
        list.add(400);
        list.remove(0);
        list.set(0, 777);
        System.out.println(list);
    }


}
