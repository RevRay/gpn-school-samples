package day7;

import java.util.Scanner;

public class WrapperDemo {

    public static void main(String[] args) {

//        Stack<int> stack;
        Stack<Integer> stack; //int

        Integer i = new Integer(7);
        Integer i2 = 90; //Integer int

        int i3 = new Integer(90);

        //autoboxing auto unboxing

        String userInput = "67";
        int result = Integer.parseInt(userInput);

        //int -2147468000

//        for (int j = Integer.MIN_VALUE; j <= Integer.MAX_VALUE; j++) {
//            System.out.println(j);
//        }

//        Scanner sc = new Scanner(System.in);
//        long money = sc.nextLong();

//        if (money > Integer.MAX_VALUE) {
//            System.err.println("error integer too big");
//        }
//        int userAccountMoney = (int) money;


        Double double1;
        Boolean boolean1;

        Integer i22 = 10;
        Integer i23 = 10;
        Integer i24 = 1090;
        Integer i25 = 1090;

        System.out.println(i22 == i23);
        System.out.println(i24 == i25);

    }

}
