package day7;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ForEach {
    public static void main(String[] args) {
        Stack<String> stack = new Stack<>(1000);
        stack.push("abc");
        stack.push("test");
        stack.push("exit");

        Iterator<String> iter1 = stack.iterator();
        while(iter1.hasNext()) {
            String currentElement = iter1.next();
            System.out.println(currentElement);
        }

//        for (String s : stack) { //?? how to make applicable?
//            System.out.println(s);
//        }

        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(51);
        list.add(14);
                                  //[    ^        ]

        //1
        Iterator<Integer> iter = list.iterator();
        while (iter.hasNext()) {
            Integer currentElement = iter.next();

            System.out.println(currentElement);
        }

        //2
        for (Integer currentElement : list) {
            System.out.println(currentElement);
            list.add(23);
        }


    }
}
