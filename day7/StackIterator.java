package day7;

import java.util.Iterator;

public class StackIterator<T> implements Iterator {

    Stack<T> stack;
    int counter = 0;

    public StackIterator(Stack<T> stack) {
        this.stack = stack;
    }

    @Override
    public boolean hasNext() {
//        if (stack.stackTop == stack.elements.length - 1) {
//            return false;
//        }
//        return true;
        return !(counter == stack.stackTop + 1);
    }

    @Override
    public T next() {
        return (T) stack.elements[counter++];
    }

}
