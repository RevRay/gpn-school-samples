package day7;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ListDemo {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
//        list.add(60);
//        list.add(50);
//        list.add(50);
//        list.add(51);
        list.add(-90);
        list.add(-97);
        list.add(-1000);
        System.out.println(list);

        int[] intArray = {1,4,3};


        System.out.println("first element " + list.get(0));
        System.out.println("max element " + findMaxValue(list));

        LinkedList<String> guestNames = new LinkedList<>();
        guestNames.add("Charlie");
        guestNames.add("Steve");
        guestNames.add("Robert");


        LinkedList<String> guestsPassedNames = new LinkedList<>();
        guestsPassedNames.add("Robert");
        guestsPassedNames.add("Steve");

        System.out.println("before removing" + guestNames);
        guestNames.removeAll(guestsPassedNames);
        guestNames.addAll(List.of("Alex", "Bob"));

        System.out.println(guestNames);

        System.out.println("after removing" + guestNames);



//        guestNames.set(1, "George");
//        guestNames.remove(3);
//        guestNames.remove("Robert");

        System.out.println(guestNames);

        String visitorName = "Steve";
        boolean b = guestNames.contains(visitorName);
        System.out.println(b);

//        LinkedList<String> l = new LinkedList();
//        l.add("d");
//        l.get(0);
//        l.remove();
//        l.set
    }

//    public static int findMaxValue(List<Integer> list) {
//        int max = 0;
//        for (int i = 0; i < list.size(); i++) {
//            if (max < list.get(i)) {
//                max = list.get(i);
//            }
//        }
//
//        return max;
//    }
    public static int findMaxValue(List<Integer> list) {
        int max = Integer.MIN_VALUE;
        for (Integer i : list) {
            max = max < i ? i : max;
        }
        return max;
    }
}
