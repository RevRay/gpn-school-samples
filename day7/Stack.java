package day7;

import java.util.Arrays;
import java.util.Iterator;

//стек целых чисел
// 12 0 3 43 155
// 0  0 0  0   0  0  0  0  0  0
public class Stack<T> implements Iterable {

    T[] elements; //10 = 20
    int stackTop;

    public Stack(int stackSize) {
        this.elements = (T[]) new Object[stackSize];
        this.stackTop = -1;
    }

    //gets values
    public T pop() {
        if (stackTop == -1) {
            System.out.println("Stack is empty!");
        }
        return elements[stackTop--];
    }

    public void push(T newElement) {
        if (stackTop >= elements.length - 1) {
            System.out.println("Stack size exceeded");
            return;
        }
        elements[++stackTop] = newElement;
    }

    @Override
    public String toString() {
        return "Stack{" +
                "elements=" + Arrays.toString(elements) +
                ", stackTop=" + stackTop +
                '}';
    }

    @Override
    public Iterator<T> iterator() {
        return new StackIterator<T>(this);
    }

    public T[] getElements() {
        return elements;
    }

    public void setElements(T[] elements) {
        this.elements = elements;
    }

    public int getStackTop() {
        return stackTop;
    }

    public void setStackTop(int stackTop) {
        this.stackTop = stackTop;
    }
}
