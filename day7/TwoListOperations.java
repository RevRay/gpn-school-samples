package day7;

import java.util.LinkedList;

public class TwoListOperations {
    public static void main(String[] args) {
        //список приглашенных - те кто принял приглашения - с этим списком сверяется консьерж
        LinkedList<String> guestListNames = new LinkedList<>();
        guestListNames.add("Charlie");
        guestListNames.add("Steve");
        guestListNames.add("Robert");
        guestListNames.add("Catherine");
        guestListNames.add("Simon");
        guestListNames.add("Dave");
        guestListNames.add("Jessica");

        //список тех кто отклонил приглашение и точно не придет
        LinkedList<String> declinedNames = new LinkedList<>();
        declinedNames.add("Martin");
        declinedNames.add("Joseph");

        //список уже прошедших гостей
        LinkedList<String> guestPassedNames = new LinkedList<>();
        guestPassedNames.add("Robert");
        guestPassedNames.add("Steve");
        guestPassedNames.add("Clark"); //человек, которого нет в списке приглашенных, но который как-то прошел


        //теперь пробуем решить несколько задачек:
        //1. как нам получить список из всех людей, кому были высланы приглашения? (список принявших приглашение + список отклонивших приглашение)

        //2. как нам получить список гостей, которые находятся на празднике без приглашения? (список прошедших гостей - список принявших приглашение)

        //3. как нам получить список гостей из списка приглашенных, кто уже прошел?

        //решить с помощью методов removeAll, addAll, retainAll

    }
}
