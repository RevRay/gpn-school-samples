package day7.hashcode;

public class Launcher {
    public static void main(String[] args) {
        Apple firstApple = new Apple(140, "Golden");
        Apple secondApple = new Apple(140, "Golden");
        Apple thirdApple = new Apple(130, "Fudzi");

        System.out.println(firstApple == secondApple);
        System.out.println(firstApple.equals(secondApple));
        System.out.println(firstApple.equals(thirdApple));

        String s = "Pineapple1";

        System.out.println(firstApple.equals(s));
        System.out.println(firstApple.equals(null));
        System.out.println(firstApple.equals(firstApple));
    }
}
