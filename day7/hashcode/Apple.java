package day7.hashcode;

import java.util.Objects;

public class Apple {
    int weight;
    String sort;

    double id = Math.random();

    public Apple(int weight, String sort) {
        this.weight = weight;
        this.sort = sort;
    }

    @Override
    public String toString() {
        return "Apple{" +
                "weight=" + weight +
                ", sort='" + sort + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Apple apple = (Apple) o;
        return weight == apple.weight && Objects.equals(sort, apple.sort);
    }

    @Override
    public int hashCode() {
        return Objects.hash(weight, sort);
    }
}
